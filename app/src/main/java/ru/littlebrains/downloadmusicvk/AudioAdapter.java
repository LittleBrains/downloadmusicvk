package ru.littlebrains.downloadmusicvk;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import ru.littlebrains.downloadmusicvk.utils.InfinityRecycleAdapter;
import ru.littlebrains.downloadmusicvk.utils.Utils;
import trikita.log.Log;

/**
 * Created by evgeniy on 16.09.2016.
 */
public class AudioAdapter extends InfinityRecycleAdapter<AudioModel> {

    public AudioAdapter(Activity activity, List<AudioModel> dataList, Reload reload) {
        super(activity, dataList, reload);
    }

    @Override
    public RecyclerView.ViewHolder getViewHolderItem(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_audio, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.artist.setText(dataList.get(position).artist);
            viewHolder.title.setText(dataList.get(position).title);
            if(dataList.get(position).url.length() == 0){
                viewHolder.btnDownload.setVisibility(View.GONE);
                return;
            }
            viewHolder.btnDownload.setVisibility(View.VISIBLE);
            final File f = new File(Utils.getPath() + dataList.get(position).artist + " - " + dataList.get(position).title + ".mp3");
            viewHolder.btnDownload.setImageResource(R.drawable.ic_action_file_file_download);
            if(f.exists() && !f.isDirectory()) {
                viewHolder.btnDownload.setVisibility(View.GONE);
                viewHolder.btnDelete.setVisibility(View.VISIBLE);
                viewHolder.btnPlay.setVisibility(View.VISIBLE);
            } else{
                viewHolder.btnPlay.setVisibility(View.INVISIBLE);
                viewHolder.btnDelete.setVisibility(View.GONE);
                viewHolder.btnDownload.setVisibility(View.VISIBLE);
                viewHolder.btnDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewHolder.btnDownload.setVisibility(View.GONE);
                        viewHolder.progressBar.setVisibility(View.VISIBLE);
                        downloadAudio(viewHolder, dataList.get(position).artist + " - " + dataList.get(position).title , dataList.get(position).url);
                    }
                });
            }

            viewHolder.btnPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.parse(Utils.getPath() + dataList.get(position).artist + " - " + dataList.get(position).title + ".mp3");
                    intent.setDataAndType(uri, "audio/mp3");
                    mActivity.startActivity(Intent.createChooser(intent, "Open audio"));
                }
            });

            viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(f.delete()){
                        Uri uri = Uri.fromFile(f);
                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
                        mActivity.sendBroadcast(intent);
                        Log.d("ACTION_MEDIA_SCANNER_SCAN_FILE");
                        viewHolder.btnDelete.setVisibility(View.GONE);
                        viewHolder.btnDownload.setVisibility(View.VISIBLE);
                        viewHolder.btnPlay.setVisibility(View.INVISIBLE);
                        viewHolder.btnDownload.setImageResource(R.drawable.ic_action_file_file_download);
                        viewHolder.btnDownload.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                viewHolder.btnDownload.setVisibility(View.GONE);
                                viewHolder.progressBar.setVisibility(View.VISIBLE);
                                Log.d("uri", dataList.get(position).url);
                                downloadAudio(viewHolder, dataList.get(position).artist + " - " + dataList.get(position).title , dataList.get(position).url);
                            }
                        });
                    }else{
                        System.out.println("Delete operation is failed.");
                    }
                }
            });
        } else {
            onBindFooterViewHolder(holder, position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageButton btnDelete;
        private ImageButton btnPlay;
        public View rootView;
        public TextView artist;
        public TextView title;
        public ProgressBar progressBar;
        public ImageButton btnDownload;

        public ViewHolder(View v) {
            super(v);
            rootView = v;
            artist = (TextView) v.findViewById(R.id.artist);
            title = (TextView) v.findViewById(R.id.title);
            btnDownload = (ImageButton) v.findViewById(R.id.btn_download);
            btnPlay = (ImageButton) v.findViewById(R.id.play);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar2);
        }
    }

    private void downloadAudio(final ViewHolder viewHolder, String nameFile, String url){
        Log.d("isExternalStorageWritable", isExternalStorageWritable());

        File directory = new File(Utils.getPath());
        directory.mkdirs();
        final File file = new File(directory, nameFile + ".mp3");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
        }
        Ion.with(mActivity)
                .load(url)
                .progressBar(viewHolder.progressBar)
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                    }
                })
                .write(fos)
                .setCallback(new FutureCallback<FileOutputStream>() {
                    @Override
                    public void onCompleted(Exception e, FileOutputStream result) {
                        if (e != null) {
                            file.delete();
                            Log.d(e);
                            viewHolder.btnDownload.setVisibility(View.VISIBLE);
                            viewHolder.btnDownload.setImageResource(R.drawable.ic_action_alert_error);
                            viewHolder.progressBar.setVisibility(View.GONE);
                            return;
                        }
                        viewHolder.btnDownload.setVisibility(View.GONE);
                        viewHolder.btnDelete.setVisibility(View.VISIBLE);
                        viewHolder.progressBar.setVisibility(View.GONE);
                        viewHolder.btnPlay.setVisibility(View.VISIBLE);

                        Uri uri = Uri.fromFile(file);
                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
                        mActivity.sendBroadcast(intent);
                        Log.d("ACTION_MEDIA_SCANNER_SCAN_FILE");
                    }
                });

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


}
