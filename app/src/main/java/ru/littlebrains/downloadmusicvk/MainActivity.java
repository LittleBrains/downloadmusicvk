package ru.littlebrains.downloadmusicvk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.littlebrains.downloadmusicvk.fragment.AuthFragment;
import ru.littlebrains.downloadmusicvk.fragment.MainFragment;
import ru.littlebrains.downloadmusicvk.utils.EndlessRecyclerOnScrollListener;
import ru.littlebrains.downloadmusicvk.utils.InfinityRecycleAdapter;
import trikita.log.Log;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       /* String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());

        Log.d("fingerprints" , fingerprints);*/

        //VKSdk.logout();
        FragmentController.setVars(getSupportFragmentManager(), this);
        if(VKSdk.isLoggedIn()) {
            FragmentController.newFragment(new MainFragment(), R.layout.fragment_main, false);
        }else{
            FragmentController.newFragment(new AuthFragment(), R.layout.fragment_auth, false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                // Пользователь успешно авторизовался
                Log.d("onResult");
                FragmentController.newFragment(new MainFragment(), R.layout.fragment_main, false);
            }
            @Override
            public void onError(VKError error) {
// Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                Log.d("onError");
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
