package ru.littlebrains.downloadmusicvk.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.littlebrains.downloadmusicvk.AudioAdapter;
import ru.littlebrains.downloadmusicvk.AudioModel;
import ru.littlebrains.downloadmusicvk.R;
import ru.littlebrains.downloadmusicvk.utils.EndlessRecyclerOnScrollListener;
import ru.littlebrains.downloadmusicvk.utils.InfinityRecycleAdapter;
import ru.littlebrains.downloadmusicvk.utils.Utils;
import trikita.log.Log;

/**
 * Created by evgeniy on 24.09.2016.
 */
public class SearchFragment extends Fragment {

    private View rootView;
    private RecyclerView recycleView;
    private AudioAdapter mAdapter;
    private static final int COUNT_ITEM_PER_PAGE = 20;
    private int page = 0;
    private String text= "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.show();
        actionBar.setTitle("Поиск");
        setHasOptionsMenu(true);

        if (rootView != null) return rootView;

        rootView = inflater.inflate(R.layout.fragment_main, null);

        recycleView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        recycleView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getContext());
        LinearLayoutManager layoutManager = (LinearLayoutManager) mLayoutManager;
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(mLayoutManager);
        recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadDataList();
            }
        });

        List<AudioModel> list = new ArrayList<>();
        mAdapter = new AudioAdapter(getActivity(), list, new InfinityRecycleAdapter.Reload(){
            @Override
            public void onReload() {
                loadDataList();
            }
        });
        recycleView.setAdapter(mAdapter);

        //loadDataList();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.open_folder:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(Utils.getPath());
                intent.setDataAndType(uri, "audio/mp3");
                startActivity(Intent.createChooser(intent, getContext().getString(R.string.open_player)));
                return false;
            default:
                break;
        }
        return false;
    }

    private void loadDataList(){
        Log.d("page", page);
        VKParameters params = VKParameters.from(VKApiConst.Q, text,
                VKApiConst.COUNT, COUNT_ITEM_PER_PAGE,
                VKApiConst.OFFSET, COUNT_ITEM_PER_PAGE * page);
        VKRequest request = VKApi.audio().get(params);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
//Do complete stuff
                Log.d(response.json);
                try {
                    final JSONArray items = response.json.getJSONObject("response").getJSONArray("items");
                    mAdapter.setServerListSize(response.json.getJSONObject("response").getInt("count"));
                    for(int i = 0; i < items.length(); i++){
                      /*  AudioModel audioModel = new AudioModel();
                        audioModel.name = getName(items.getJSONObject(i));
                        audioModel.url = getUrl(items.getJSONObject(i));
                        Log.d(audioModel.name, audioModel.url);
                        mAdapter.addItem(audioModel);*/
                    }
                    //downloadAudio(getUrl(items.getJSONObject(index)), getName(items.getJSONObject(index)));

                }catch (JSONException e){

                }
                page++;
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onError(VKError error) {
//Do error stuff
                Log.d("onError", error.errorCode, error.errorMessage);
            }
            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
//I don't really believe in progress
            }
        });
    }

    private String getName(JSONObject object) throws JSONException{
        return object.getString("artist") + " - " + object.getString("title").trim();
    }
    private String getUrl(JSONObject object) throws JSONException{
        return object.getString("url");
    }
}
