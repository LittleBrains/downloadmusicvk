package ru.littlebrains.downloadmusicvk.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.littlebrains.downloadmusicvk.AudioAdapter;
import ru.littlebrains.downloadmusicvk.AudioModel;
import ru.littlebrains.downloadmusicvk.FragmentController;
import ru.littlebrains.downloadmusicvk.MainActivity;
import ru.littlebrains.downloadmusicvk.R;
import ru.littlebrains.downloadmusicvk.utils.EndlessRecyclerOnScrollListener;
import ru.littlebrains.downloadmusicvk.utils.InfinityRecycleAdapter;
import ru.littlebrains.downloadmusicvk.utils.Utils;
import trikita.log.Log;

/**
 * Created by evgeniy on 24.09.2016.
 */
public class MainFragment extends Fragment {

    private View rootView;
    private RecyclerView recycleView;
    private AudioAdapter mAdapter;
    private static final int COUNT_ITEM_PER_PAGE = 20;
    private int page = 0;
    private String search = "";
    private boolean isLoad = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.show();
        actionBar.setTitle("Моя музыка");
        setHasOptionsMenu(true);

        if (rootView != null) return rootView;

        rootView = inflater.inflate(R.layout.fragment_main, null);

        recycleView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        recycleView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getContext());
        LinearLayoutManager layoutManager = (LinearLayoutManager) mLayoutManager;
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(mLayoutManager);
        recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadDataList();
            }
        });

        List<AudioModel> list = new ArrayList<>();
        mAdapter = new AudioAdapter(getActivity(), list, new InfinityRecycleAdapter.Reload(){
            @Override
            public void onReload() {
                loadDataList();
            }
        });
        recycleView.setAdapter(mAdapter);

        //loadDataList();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        final MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        final MenuItem openFolderItem = menu.findItem(R.id.open_folder);
        final SearchView searchView = (SearchView) mSearchMenuItem.getActionView();
        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                openFolderItem.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                openFolderItem.setVisible(true);
                search = "";
                page = 0;
                loadDataList();
                return true;
            }
        });
        MenuItemCompat.setActionView(mSearchMenuItem, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = query;
                page = 0;
                loadDataList();

                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.open_folder:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(Utils.getPath());
                intent.setDataAndType(uri, "audio/mp3");
                startActivity(Intent.createChooser(intent, getContext().getString(R.string.open_player)));
                return false;
            case R.id.action_search:

                break;
            default:
                break;
        }
        return false;
    }

    private void loadDataList(){
        Log.d("page", page, search);
        if(isLoad) return;
        isLoad = true;
        VKParameters params;
        VKRequest request;
        if(page == 0){
            mAdapter.clear();
            mAdapter.notifyDataSetChanged();
        }
        if(search.length() > 0){
            params= VKParameters.from(VKApiConst.Q, search,
                    VKApiConst.AUTO_COMPLETE, 0,
                    "search_own", 1,
                    VKApiConst.COUNT, COUNT_ITEM_PER_PAGE,
                    VKApiConst.OFFSET, COUNT_ITEM_PER_PAGE * page);
            request = VKApi.audio().search(params);
        }else {
            params = VKParameters.from(VKApiConst.OWNER_ID, VKAccessToken.currentToken().userId,
                    VKApiConst.COUNT, COUNT_ITEM_PER_PAGE,
                    VKApiConst.OFFSET, COUNT_ITEM_PER_PAGE * page);
            request = VKApi.audio().get(params);
        }

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d(response.json);
                try {
                    final JSONArray items = response.json.getJSONObject("response").getJSONArray("items");
                    mAdapter.setServerListSize(response.json.getJSONObject("response").getInt("count"));
                    for(int i = 0; i < items.length(); i++){
                        AudioModel audioModel = new AudioModel();
                        audioModel.artist = items.getJSONObject(i).getString("artist");
                        audioModel.title = items.getJSONObject(i).getString("title");
                        audioModel.url = items.getJSONObject(i).getString("url");
                        Log.d(audioModel.artist, audioModel.url);
                        mAdapter.addItem(audioModel);
                    }
                    //downloadAudio(getUrl(items.getJSONObject(index)), getName(items.getJSONObject(index)));

                }catch (JSONException e){

                }
                page++;
                mAdapter.notifyDataSetChanged();
                isLoad = false;
            }
            @Override
            public void onError(VKError error) {
                Log.d("onError", error.errorCode, error.errorMessage);
                isLoad = false;
            }
            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                isLoad = false;
            }
        });
    }
}
