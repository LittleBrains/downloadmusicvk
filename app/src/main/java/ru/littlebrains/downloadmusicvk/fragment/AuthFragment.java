package ru.littlebrains.downloadmusicvk.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import ru.littlebrains.downloadmusicvk.FragmentController;
import ru.littlebrains.downloadmusicvk.R;
import trikita.log.Log;

/**
 * Created by evgeniy on 24.09.2016.
 */
public class AuthFragment extends Fragment {

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setShowHideAnimationEnabled(false);
        actionBar.hide();

        if (rootView != null) return rootView;

        rootView = inflater.inflate(R.layout.fragment_auth, null);

        Button btn1 = (Button) rootView.findViewById(R.id.button_auth);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.login(getActivity(), "audio");
            }
        });


        return rootView;
    }


}
