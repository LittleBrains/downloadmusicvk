package ru.littlebrains.downloadmusicvk.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.inputmethod.InputMethodManager;

import trikita.log.Log;

public class Utils {
    private static String STORAGE_NAME = "";


    public static SharedPreferences.Editor getSharedPreferencesEditor(
            Context mContext) {
        setStorageName(mContext);
        return mContext
                .getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
                .edit();
    }

    public static SharedPreferences getSharedPreferences(Context mContext) {
        setStorageName(mContext);
        return mContext
                .getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
    }

    private static void setStorageName(Context context){
        STORAGE_NAME = "DowndloadMusicVK_LB";
    }

    public static boolean isInternetOn(Context mContext) {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            Log.d("", "Internet enable");
            return true;
        }
        Log.d("", "Internet desable");
        return false;
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void showKeyboard(Context mContext) {
        ((InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static String getPath(){
        Log.d(Environment.getExternalStorageDirectory().getPath() + "/Music/Download VK/");
        return Environment.getExternalStorageDirectory().getPath() + "/Music/Download VK/";
    }
}
