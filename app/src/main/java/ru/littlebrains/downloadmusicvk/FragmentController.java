package ru.littlebrains.downloadmusicvk;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import ru.littlebrains.downloadmusicvk.utils.Utils;


public class FragmentController {

	private static final String TAG = "FragmentController";
	public static FragmentManager mFragmentManager;
	public static AppCompatActivity mActivity;
	//public static ActionBarDrawerToggle mDrawerToggle;

	private static void checkVars(){
		if(mFragmentManager == null || mActivity == null){
			throw new NullPointerException("FragmentController have vars equals NULL");
		}
	}

	public static void setVars(FragmentManager fragmentManager, AppCompatActivity activity){
		mFragmentManager = fragmentManager;
		mActivity = activity;

		mFragmentManager.addOnBackStackChangedListener(backStackListener);
	}
	private static FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
		@Override
		public void onBackStackChanged() {
			setNavIcon();
		};
	};

	protected static void setNavIcon() {
		int backStackEntryCount = mFragmentManager.getBackStackEntryCount();
	}


	public static void newFragment(Fragment fragment, int tag, boolean canGoBack) {
		newFragment(fragment, String.valueOf(tag), canGoBack);
	}
	
	public static void newFragment(final Fragment fragment, final String tag, final boolean canGoBack) {
		checkVars();
		final int WHAT = 1;
		Handler handler = new Handler(){
		    @Override
		    public void handleMessage(Message msg) {                    
		        if(msg.what == WHAT) changeFragment(fragment, tag, canGoBack);            
		    }
		};
		handler.sendEmptyMessage(WHAT);
		
	}
	
	private static void changeFragment(Fragment fragment, String tag, boolean canGoBack) {
		checkVars();
		Utils.hideKeyboard(mActivity);
		//mDrawerToggle.setDrawerIndicatorEnabled(!canGoBack);
		//mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(canGoBack);
		tag+=Math.random();
		if(canGoBack){
			mFragmentManager.beginTransaction().replace(R.id.container, fragment, tag).addToBackStack(null).commit();
		} else{
			for(int i = 0; i < mFragmentManager.getBackStackEntryCount(); ++i) {
				mFragmentManager.popBackStack();
			}
			mFragmentManager
					.beginTransaction()
					.replace(R.id.container, fragment, tag)
					.commit();
		}
	}

	public static void clearBackStack(){
		checkVars();
		mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		Utils.hideKeyboard(mActivity);
	}

	public static boolean backFragmet() {
		checkVars();
		Utils.hideKeyboard(mActivity);
		final int WHAT = 1;
		Handler handler = new Handler(){
		    @Override
		    public void handleMessage(Message msg) {
				try {
					if (msg.what == WHAT) mFragmentManager.popBackStack();
				}catch(IllegalStateException e){

				}
		    }
		};
		if (mFragmentManager.getBackStackEntryCount() == 1) {
			//mDrawerToggle.setDrawerIndicatorEnabled(true);
			//mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		}
		handler.sendEmptyMessage(WHAT);

		if(mFragmentManager.getBackStackEntryCount() > 0){
			return false;
		}else {
			return true;
		}
	}

	public static void refreshMenu() {
		checkVars();
		mActivity.invalidateOptionsMenu();
	}
}
